import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {AllProductsComponent, ProductDetailsComponent} from './admin/product/all-products/all-products.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MaterialModule} from './material/material.module';
import {HttpClientModule} from '@angular/common/http';
import {MAT_DIALOG_DEFAULT_OPTIONS, MatPaginatorIntl} from '@angular/material';
import { MainWindowComponent } from './admin/main-window/main-window.component';
import {CustomPaginator} from './custom-paginator';
import { AddProductComponent } from './admin/product/add-product/add-product.component';
import {AllClientsComponent, ClientDetailsComponent} from './admin/client/all-clients/all-clients.component';
import { AddClientComponent } from './admin/client/add-client/add-client.component';
import {AllTransactionsComponent, TransactionDetailsComponent} from './admin/transaction/all-transactions/all-transactions.component';
import { AddTransactionComponent } from './admin/transaction/add-transaction/add-transaction.component';
import { AddProductToTransactionComponent } from './admin/transaction/add-product-to-transaction/add-product-to-transaction.component';
import { StatisticsComponent } from './admin/statistics/statistics.component';
import { PickFiltersComponent } from './admin/statistics/pick-filters/pick-filters.component';
import { LoginComponent } from './security/login/login.component';
import { RegisterComponent } from './security/register/register.component';
import { UserComponent } from './security/user/user.component';
import { httpInterceptorProviders } from './auth/auth-interceptor';
import { MainPageComponent } from './shop/main-page/main-page.component';
import { NavbarComponent } from './navbar/navbar.component';
import { BuyProductComponent } from './shop/buy-product/buy-product.component';
import { EncourageLoginComponent } from './shop/encourage-login/encourage-login.component';
import { HistoryComponent } from './shop/history/history.component';

@NgModule({
  declarations: [
    AppComponent,
    AllProductsComponent,
    ProductDetailsComponent,
    MainWindowComponent,
    AddProductComponent,
    AddClientComponent,
    AllClientsComponent,
    ClientDetailsComponent,
    AllTransactionsComponent,
    TransactionDetailsComponent,
    AddTransactionComponent,
    AddProductToTransactionComponent,
    StatisticsComponent,
    PickFiltersComponent,
    LoginComponent,
    RegisterComponent,
    UserComponent,
    MainPageComponent,
    NavbarComponent,
    BuyProductComponent,
    EncourageLoginComponent,
    HistoryComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule
  ],
  providers: [
    {provide: MAT_DIALOG_DEFAULT_OPTIONS, useValue: {hasBackdrop: true}},
    {provide: MatPaginatorIntl, useValue: CustomPaginator() },
    httpInterceptorProviders
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    ProductDetailsComponent,
    AddProductComponent,
    AddClientComponent,
    ClientDetailsComponent,
    TransactionDetailsComponent,
    AddTransactionComponent,
    AddProductToTransactionComponent,
    PickFiltersComponent,
    BuyProductComponent,
    EncourageLoginComponent,
    ]
})
export class AppModule { }
