import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Client} from '../admin/client/client';
import {Transaction} from '../admin/transaction/transaction';

@Injectable({
  providedIn: 'root'
})
export class ShopProviderService {
  private base = "http://localhost:8080/api/shop";

  constructor(private http: HttpClient) { }

  whoAmI() {
    return this.http.get<Client>(`${this.base}/whoami`);
  }

  getHistory() {
    return this.http.get<Transaction[]>(`${this.base}/history`);
  }

  buyProduct(productId: number){
    return this.http.get<void>(`${this.base}/buy/${productId}`);
  }
}
