import {Component, Inject, OnInit} from '@angular/core';
import {Product} from '../../admin/product/product';
import {MAT_DIALOG_DATA} from '@angular/material';
import {ShopProviderService} from '../shop-provider.service';

@Component({
  selector: 'app-buy-product',
  templateUrl: './buy-product.component.html',
  styleUrls: ['./buy-product.component.css']
})
export class BuyProductComponent implements OnInit {

  private product: Product;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: Product,
    private shopProvider: ShopProviderService
  ) {
    this.product = data;
  }

  ngOnInit() {
  }

  buy() {
    this.shopProvider.buyProduct(this.product.id).subscribe(
      result => console.log(`item with id ${this.product.id} bought!`)
    )
  }
}
