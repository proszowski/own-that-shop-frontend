import { Component, OnInit } from '@angular/core';
import {TokenStorageService} from '../../auth/token-storage.service';
import {ClientsProviderService} from '../../admin/client/clients-provider/clients-provider.service';
import {ShopProviderService} from '../shop-provider.service';
import {MatTableDataSource} from '@angular/material';
import {ProductsProviderService} from '../../admin/product/products-provider/products-provider.service';
import {Transaction} from '../../admin/transaction/transaction';

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.css']
})
export class HistoryComponent implements OnInit {
  username: string;
  displayedColumns = ['date', 'product', 'totalPrice'];
  dataSource: any;
  products = {};

  constructor(private shopProvider: ShopProviderService,
              private productsProvider: ProductsProviderService) { }

  ngOnInit() {
    this.dataSource = new MatTableDataSource();
    this.shopProvider.whoAmI().subscribe(
      client => this.username = client.firstName
    );

    this.shopProvider.getHistory().subscribe(
      result => {
        this.dataSource = result;
        var i = 0;
        for(i ; i < result.length; i++){
          const productId = result[i].transactionDetails[0].productId;
          this.productsProvider.getProductWithId(productId).subscribe(
            product => {
              this.products[productId] = product.name;
              console.log(this.products);
            }
          );
        }
      }
    )
  }

}
