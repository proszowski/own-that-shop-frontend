import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EncourageLoginComponent } from './encourage-login.component';

describe('EncourageLoginComponent', () => {
  let component: EncourageLoginComponent;
  let fixture: ComponentFixture<EncourageLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EncourageLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EncourageLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
