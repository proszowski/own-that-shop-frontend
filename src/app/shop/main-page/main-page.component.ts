import {Component, OnInit} from '@angular/core';
import {TokenStorageService} from '../../auth/token-storage.service';
import {Product} from '../../admin/product/product';
import {ProductsProviderService} from '../../admin/product/products-provider/products-provider.service';
import {MatDialog} from '@angular/material';
import {BuyProductComponent} from '../buy-product/buy-product.component';
import {LoginComponent} from '../../security/login/login.component';
import {EncourageLoginComponent} from '../encourage-login/encourage-login.component';

@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.css']
})
export class MainPageComponent implements OnInit {
  products: Product[];
  productsFetched = false;

  constructor(private token: TokenStorageService,
              private productsProvider: ProductsProviderService,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.productsProvider.getAllProducts().subscribe(
      result => {
        this.products = result;
        this.productsFetched = true;
      }
    );
  }

  buyProduct(product: Product) {
    if (this.token.getAuthorities().length != 0) {
      this.showDialogBuyProduct(product);
    } else {
      this.showDialogLogIn();
    }
  }

  showDialogBuyProduct(product: Product) {
    const dialogRef = this.dialog.open(BuyProductComponent, {
      data: product,
      height: '50vh',
      width: '45vw',
    });
  }

  showDialogLogIn() {
    const dialogRef = this.dialog.open(EncourageLoginComponent, {
      height: '50vh',
      width: '45vw',
    });
  }
}
