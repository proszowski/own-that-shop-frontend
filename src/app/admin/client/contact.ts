export class Contact {
  id: number;
  phoneNumber: number;
  email: string;

  constructor(contact: Contact) {
    if (contact != null) {
      this.id = contact.id;
      this.phoneNumber = contact.phoneNumber;
      this.email = contact.email;
    }
  }
}
