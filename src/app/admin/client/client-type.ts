export class ClientType {
  id: number;
  name: string;

  constructor(clientType: ClientType) {
    if (clientType != null) {
      this.id = clientType.id;
      this.name = clientType.name;
    }
  }
}
