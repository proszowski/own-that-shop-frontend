export class Company {
  constructor(company: Company) {
    if(company != null){
      this.id = company.id;
      this.name = company.name;
    }
  }

  id: number;
  name: string;
}
