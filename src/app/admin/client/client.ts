import {Address} from './address';
import {Contact} from './contact';
import {ClientType} from './client-type';
import {Company} from './company';

export class Client {
    constructor(client: Client) {
      if(client != null){
        this.id = client.id;
        this.firstName = client.firstName;
        this.surname = client.surname;
        this.company = new Company(client.company);
        this.address = new Address(client.address);
        this.contact = new Contact(client.contact);
        this.clientType = new ClientType(client.clientType);
      } else {
        this.address = new Address(null);
        this.contact = new Contact(null);
        this.clientType = new ClientType(null);
        this.company = new Company(null);
      }
    }
    id: number;
    firstName: string;
    surname: string;
    company: Company;
    address: Address;
    contact: Contact;
    clientType: ClientType;
  }
