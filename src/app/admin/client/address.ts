export class Address {
  constructor(address: Address) {
    if(address != null){
      this.id = address.id;
      this.street = address.street;
      this.city = address.city;
      this.country = address.country;
      this.houseNumber = address.houseNumber;
    }
  }

  id: number;
  street: string;
  city: string;
  country: string;
  houseNumber: number;
}
