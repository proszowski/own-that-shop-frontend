import { Injectable } from '@angular/core';
import {ProductsProviderService} from '../product/products-provider/products-provider.service';
import {FlatTransactionDetails} from './flat-transaction-details';
import {TransactionDetails} from './transaction-details';

@Injectable({
  providedIn: 'root'
})
export class TransactionDetailsMapperService {

  constructor(private productsProvider: ProductsProviderService) {
  }

  fromFlat(flatTransactionDetails: FlatTransactionDetails): TransactionDetails {
    const transactionDetails: TransactionDetails = new TransactionDetails();
    transactionDetails.id = flatTransactionDetails.id;
    transactionDetails.quantity = flatTransactionDetails.quantity;
    transactionDetails.transactionId = flatTransactionDetails.transactionId;
    transactionDetails.productId = flatTransactionDetails.product.id;
    transactionDetails.totalPrice = flatTransactionDetails.totalPrice;
    return transactionDetails;
  }

  toFlat(transactionDetails: TransactionDetails): FlatTransactionDetails {
    const flatTransactionDetails: FlatTransactionDetails = new FlatTransactionDetails();
    this.productsProvider.getProductWithId(transactionDetails.productId).subscribe(result => flatTransactionDetails.product = result);
    flatTransactionDetails.totalPrice = transactionDetails.totalPrice;
    flatTransactionDetails.transactionId = transactionDetails.transactionId;
    flatTransactionDetails.id = transactionDetails.id;
    flatTransactionDetails.quantity = transactionDetails.quantity;
    return flatTransactionDetails;
  }
}
