export class TransactionDetails {
  id: number;
  productId: number;
  quantity: number;
  totalPrice: number;
  transactionId: number;
}
