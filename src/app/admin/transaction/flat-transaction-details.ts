import {Product} from '../product/product';

export class FlatTransactionDetails {
  id: number;
  product: Product;
  quantity: number;
  totalPrice: number;
  transactionId: number;


  constructor() {
    this.product = new Product(null);
  }
}
