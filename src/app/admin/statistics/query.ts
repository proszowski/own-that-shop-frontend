import {Filters} from './filters';

export class Query {
  filters: Filters;
  name: string;

  constructor() {
    this.filters = new Filters();
  }
}
