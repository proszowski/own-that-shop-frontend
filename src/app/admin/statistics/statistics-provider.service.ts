import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {QueryDto} from './query-dto';

@Injectable({
  providedIn: 'root'
})
export class StatisticsProviderService {

  private base = 'http://localhost:8080/api/admin/statistic';

  constructor(private http: HttpClient) { }

  getResult(result: string, queryDto: QueryDto) {
    return this.http.post(`${this.base}/${result}`, queryDto);
  }
}
