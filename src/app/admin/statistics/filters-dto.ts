import {Filters} from './filters';

export class FiltersDto {
  filterConfigurations = {};

  private constructor() {

  }

  static of(
    filters: Filters
  ): FiltersDto {

    const filtersDto = new FiltersDto();

    if (filters.country != null) {
      filtersDto.filterConfigurations['COUNTRY'] = {
        'parameters': {
          'countryName': filters.country
        }
      };
    }

    if (filters.clientType != null) {
      filtersDto.filterConfigurations['CLIENT_TYPE'] = {
        'parameters': {
          'name': filters.clientType.name
        }
      };
    }

    if (filters.client != null) {
      filtersDto.filterConfigurations['CLIENT'] = {
        'parameters': {
          'id': filters.client.id
        }
      };
    }

    if (filters.product != null) {
      filtersDto.filterConfigurations['PRODUCT'] = {
        'parameters': {
          'id': filters.product.id
        }
      };
    }

    if (filters.startDate != null) {
      filtersDto.filterConfigurations['DATE_RANGE'] = {
        'parameters': {
          'start': filters.startDate
        }
      };
    }

    if (filters.endDate != null) {
      var dateRange = filtersDto.filterConfigurations['DATE_RANGE'];
      if (dateRange != null) {
        dateRange['parameters']['end'] = filters.endDate;
      } else {
        filtersDto.filterConfigurations['DATE_RANGE'] = {
          'parameters': {
            'end': filters.endPrice
          }
        };
      }
    }

    if (filters.startPrice != null) {
      filtersDto.filterConfigurations['PRICE_RANGE'] = {
        'parameters': {
          'start': filters.startPrice
        }
      };
    }

    if (filters.endPrice != null) {
      const priceRange = filtersDto.filterConfigurations['PRICE_RANGE'];
      if (priceRange != null) {
        priceRange['parameters']['end'] = filters.endPrice;
      } else {
        filtersDto.filterConfigurations['PRICE_RANGE'] = {
          'parameters': {
            'end': filters.endPrice
          }
        };
      }
    }

    return filtersDto;
  }
}
