import { Component, OnInit } from '@angular/core';
import {MatDialog, MatTableDataSource} from '@angular/material';
import {StatisticsProviderService} from './statistics-provider.service';
import {TransactionsMapperService} from '../transaction/transactions-mapper.service';
import {PickFiltersComponent} from './pick-filters/pick-filters.component';
import {Filters} from './filters';
import {FiltersDto} from './filters-dto';
import {QueryDto} from './query-dto';
import {Query} from './query';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.css']
})
export class StatisticsComponent implements OnInit {
  dataSource = new MatTableDataSource();
  columnsToDisplay = [];
  productColumns = ['Name', 'ProductType', 'Price', 'Quantity'];
  clientColumns = ['First Name', 'Last Name', 'City', 'Country', 'Email'];
  transactionColumns = ['Id', 'Company', 'FirstName', 'Surname', 'TotalPrice'];
  desiredResult = "PRODUCTS";
  query = new Query();

  constructor(private statisticsProvider: StatisticsProviderService,
              private mapper: TransactionsMapperService,
              private dialog: MatDialog) {
    this.columnsToDisplay = this.productColumns;
  }

  ngOnInit() {
  }

  changeResult() {
    this.dataSource.data = [];
    switch(this.desiredResult){
      case "PRODUCTS":
        this.columnsToDisplay = this.productColumns;
        break;
      case "CLIENTS":
        this.columnsToDisplay = this.clientColumns;
        break;
      case "TRANSACTIONS":
        this.columnsToDisplay = this.transactionColumns;
        break;
    }
  }

  getResults() {
    this.statisticsProvider.getResult(this.desiredResult, QueryDto.of(this.query)).subscribe(
      result =>{
        console.log(result);
        var entities = result['queryResult'].map(e => e['entity']);
        if(this.desiredResult == "transactions"){
          entities = entities.map(e => this.mapper.toFlat(e))
        }
        this.dataSource.data = entities;
      }
    )
  }

  pickFilters() {
    const filters = this.query.filters;
    const dialogRef = this.dialog.open(PickFiltersComponent, {
      data: {filters},
      height: '65vh',
      width: '40vw',
    });

    dialogRef.afterClosed().subscribe(
      result => {
        if(result != null){
          this.query.filters = result;
        }
      }
    )
  }

  clearFilters() {
    this.query.filters = new Filters();
  }
}
