import {Component, Inject, OnInit} from '@angular/core';
import {ClientsProviderService} from '../../client/clients-provider/clients-provider.service';
import {ClientType} from '../../client/client-type';
import {Client} from '../../client/client';
import {Product} from '../../product/product';
import {ProductsProviderService} from '../../product/products-provider/products-provider.service';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {Filters} from '../filters';

@Component({
  selector: 'app-pick-filters',
  templateUrl: './pick-filters.component.html',
  styleUrls: ['./pick-filters.component.css']
})
export class PickFiltersComponent implements OnInit {
  availableClientTypes: ClientType[];
  availableClients: Client[];
  availableProducts: Product[];
  editableFilters: Filters;

  constructor(private clientsProvider: ClientsProviderService,
              private productsProvider: ProductsProviderService,
              public dialogRef: MatDialogRef<PickFiltersComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any) {
  }

  ngOnInit() {
    this.editableFilters = this.data.filters;
    this.clientsProvider.getAllClientTypes().subscribe(
      result => this.availableClientTypes = result
    );
    this.clientsProvider.getAllClients().subscribe(
      result => this.availableClients = result
    );
    this.productsProvider.getAllProducts().subscribe(
      result => this.availableProducts = result
    );
  }

  updateFilters() {
    this.dialogRef.close(this.editableFilters);
  }
}
