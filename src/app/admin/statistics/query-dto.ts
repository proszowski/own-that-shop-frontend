import {FiltersDto} from './filters-dto';
import {Filters} from './filters';
import {Query} from './query';

export class QueryDto {

  filtersDto: FiltersDto;

  private constructor(filters: Filters) {
    this.filtersDto = FiltersDto.of(filters);
  }

  static of(query: Query) {
    return new QueryDto(query.filters);
  }
}
