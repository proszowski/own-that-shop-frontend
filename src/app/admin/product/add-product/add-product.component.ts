import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {ProductsProviderService} from '../products-provider/products-provider.service';
import {Product} from '../product';
import {ProductType} from '../product-type';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {
  imageUrl: string;
  product: Product;
  availableProductTypes: ProductType[];
  isRequestInProgress = false;

  constructor(public dialogRef: MatDialogRef<AddProductComponent>,
              private productsProvider: ProductsProviderService) { }

  ngOnInit() {
    this.product = new Product(null);
    this.productsProvider.getAllAvailableProductTypes().subscribe(
      result => this.availableProductTypes = result
    );
  }

  loadImage() {
    this.product.image = this.imageUrl;
  }

  cancel() {
    this.dialogRef.close();
  }

  save(){
    this.isRequestInProgress = true;
    this.productsProvider.addNewProduct(this.product).subscribe(
      result => {
          this.isRequestInProgress = false;
          this.dialogRef.close(result);
      },
      error => {
        this.isRequestInProgress = false;
      },
    );
  }
}

