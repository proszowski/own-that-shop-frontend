import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Product} from '../product';
import {Observable} from 'rxjs';
import {ProductType} from '../product-type';

@Injectable({
  providedIn: 'root'
})
export class ProductsProviderService {

  private base = 'http://localhost:8080/api/product';

  constructor(private http: HttpClient) {
    this.getAllProducts();
  }

  getAllProducts(): Observable<Product[]> {
    return this.http.get<Product[]>(`${this.base}`);
  }

  updateProduct(product: Product) {
    return this.http.put<Product>(`${this.base}/update`, product);
  }

  deleteProduct(productId: number) {
    this.http.delete<number[]>(`${this.base}/delete/${productId}`).subscribe();
  }

  addNewProduct(product: Product): Observable<any> {
    return this.http.post<any>(`${this.base}/add`, product);
  }

  getProductWithId(productId: number) {
    return this.http.get<Product>(`${this.base}/${productId}`);
  }

  getAllAvailableProductTypes(): Observable<ProductType[]> {
    return this.http.get<ProductType[]>(`${this.base}/available-types`);
  }
}
