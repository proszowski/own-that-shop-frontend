import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MatTableModule} from '@angular/material/table';
import {
  MatButtonModule,
  MatCardModule, MatCheckboxModule,
  MatDialogModule, MatFormFieldModule,
  MatGridListModule, MatIconModule, MatInputModule,
  MatListModule, MatPaginatorModule,
  MatProgressSpinnerModule, MatSelectModule
} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatTableModule,
    MatListModule,
    MatCardModule,
    MatDialogModule,
    MatButtonModule,
    MatGridListModule,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    ReactiveFormsModule,
    MatIconModule,
    FormsModule,
    MatPaginatorModule,
    MatSelectModule,
  ],
  exports: [
    MatTableModule,
    MatListModule,
    MatCardModule,
    MatDialogModule,
    MatButtonModule,
    MatGridListModule,
    MatProgressSpinnerModule,
    MatFormFieldModule,
    MatInputModule,
    MatCheckboxModule,
    ReactiveFormsModule,
    MatIconModule,
    FormsModule,
    MatPaginatorModule,
    MatSelectModule,
  ]
})
export class MaterialModule { }
