import {Client} from '../admin/client/client';

export class SignUpInfo {
  client: Client;
  name: string;
  username: string;
  email: string;
  role: string[];
  password: string;

  constructor(name: string, email: string, password: string, client: Client) {
    this.name = name;
    this.username = email;
    this.email = email;
    this.password = password;
    this.role = ['user'];
    this.client = client;
  }
}
