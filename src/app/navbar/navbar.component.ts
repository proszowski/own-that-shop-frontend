import { Component, OnInit } from '@angular/core';
import {TokenStorageService} from '../auth/token-storage.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private tokenStorage: TokenStorageService) { }

  ngOnInit() {
  }

  hasNoAuthority() {
    return this.tokenStorage.getAuthorities().length == 0
  }

  signout() {
    window.sessionStorage.clear();
  }

  isAdmin() {
    return this.tokenStorage.getAuthorities().includes('ROLE_ADMIN')
  }
}
