import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {AllProductsComponent} from './admin/product/all-products/all-products.component';
import {MainWindowComponent} from './admin/main-window/main-window.component';
import {MainPageComponent} from './shop/main-page/main-page.component';
import {AllClientsComponent} from './admin/client/all-clients/all-clients.component';
import {AllTransactionsComponent} from './admin/transaction/all-transactions/all-transactions.component';
import {StatisticsComponent} from './admin/statistics/statistics.component';
import {LoginComponent} from './security/login/login.component';
import {RegisterComponent} from './security/register/register.component';
import {HistoryComponent} from './shop/history/history.component';

const routes: Routes = [
  {path: 'admin/products', pathMatch: 'full', component: AllProductsComponent},
  {path: 'admin/clients', pathMatch: 'full', component: AllClientsComponent},
  {path: 'admin/transactions', pathMatch: 'full', component: AllTransactionsComponent},
  {path: 'admin/statistics', pathMatch: 'full', component: StatisticsComponent},
  {path: 'admin', pathMatch: 'full', component: MainWindowComponent},
  {path: 'auth/login', pathMatch: 'full', component: LoginComponent},
  {path: 'auth/register', pathMatch: 'full', component: RegisterComponent},
  {path: 'history', pathMatch: 'full', component: HistoryComponent},
  {path: '', pathMatch: 'full', component: MainPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
