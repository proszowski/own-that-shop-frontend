import {Component, OnInit} from '@angular/core';
import {SignUpInfo} from '../../auth/signup-info';
import {AuthService} from '../../auth/auth.service';
import {MatDialog} from '@angular/material';
import {AddClientComponent} from '../../admin/client/add-client/add-client.component';
import {Client} from '../../admin/client/client';
import {ClientType} from '../../admin/client/client-type';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  form: any = {};
  signupInfo: SignUpInfo;
  isSignedUp = false;
  isSignUpFailed = false;
  errorMessage = '';

  constructor(private authService: AuthService,
              private dialog: MatDialog) {
  }

  ngOnInit() {
    this.form.client = new Client(null);
  }

  onSubmit() {
    this.form.client.firstName = this.form.name;
    this.form.client.contact.email = this.form.email;
    var clientType = new ClientType(null);
    clientType.name = 'hurt';
    clientType.id = 1;
    this.form.client.clientType = clientType;
    this.signupInfo = new SignUpInfo(
      this.form.name,
      this.form.email,
      this.form.password,
      this.form.client
    );

    this.authService.signUp(this.signupInfo).subscribe(
      data => {
        console.log(data);
        this.isSignedUp = true;
        this.isSignUpFailed = false;
      },
      error => {
        console.log(error);
        this.errorMessage = error.error.message;
        this.isSignUpFailed = true;
      }
    );
  }
}
